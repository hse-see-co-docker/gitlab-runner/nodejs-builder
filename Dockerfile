FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest


ARG NODE_VERSION=11.10.0
ARG NPM_VERSION=6.7.0

ADD ./install_node.sh /opt/app-root/etc/install_node.sh

RUN chmod +x /opt/app-root/etc/install_node.sh && \
    /opt/app-root/etc/install_node.sh

RUN npm i -g yarn
#ADD ./entrypoint.sh /entrypoint.sh
#RUN chmod augo+x /entrypoint.sh
ENV HOME=/opt/app-root/src
RUN mkdir /.yarn && chmod augo+rw /.yarn
WORKDIR $HOME
RUN chmod augo+rw -R ${HOME}

VOLUME /.yarn

# Gitlab Runner doesn't allow to pass arguments so we need to create aliases for better yarn usage
ADD yarn_build /usr/bin/yarn_build
ADD yarn_install /usr/bin/yarn_install
ADD yarn_test /usr/bin/yarn_test
RUN chmod +x /usr/bin/yarn_*
ENV NODE_PATH=/usr/lib/node_modules

USER 1001
CMD ["/bin/bash", "-e"]
#ENTRYPOINT "/entrypoint.sh"
